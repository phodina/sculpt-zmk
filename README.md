[!["Buy Me A Coffee"](https://www.buymeacoffee.com/assets/img/custom_images/orange_img.png)](https://www.buymeacoffee.com/phodina)

# ZMK for Microsoft Sculpt Keyboard

There are only handful of ergonomic keyboards out there. Microsoft Sculpt keyboard has been recently discontinued and there's still a lot of working keyboards out there. However, they require a special wireless dongle as they don't use Bluetooth but custom 2.4GHz proprietary protocol. Another disadvantage is that the dongle and the keyboard are paired.

Therefore the goal of this project is to create a replacement board that would allow to use the keyboard over Bluetooth as well as over USB C cable to recharge batteries.

ZMK is chosen since the idea was to run Zephyr OS due to the support of wide range of MCUs. nRF52 family is the right choice due to being low-power, bluetooth and USB enabled.

![Open source controller](data/sculpt3d.png)

![Microsoft Sculpt Keyboard](data/Microsoft_Sculpt.jpg)

## Discontinuation
- [Microsoft Killed My Favorite Keyboard, And I’m Mad About It](https://hackaday.com/2024/04/16/microsoft-killed-my-favorite-keyboard-and-im-mad-about-it/)
- [Microsoft Sculpt Keyboard discontinuation](https://answers.microsoft.com/en-us/surface/forum/all/microsoft-sculpt-keyboard-discontinuation-and/e8ad5fce-659e-4d45-ae4b-006821b84ccc)

## Keyboard Matrix
There are 30 pins on the US layout Sculpt flex cable. Pin 1 is the outermost pin (closest to the rounded corner with the writing) and lines up with the pin marked "1" underneath a triangle on the US controller PCB.

| Pin | Matrix Connection        |
| --- | ------------------------ |
| 1   | Fn Switch                |
| 2   | row 1                    |
| 3   | row 2                    |
| 4   | row 3                    |
| 5   | row 4                    |
| 6   | row 5                    |
| 7   | row 6                    |
| 8   | row 7                    |
| 9   | row 8                    |
| 10  | col A                    |
| 11  | col B                    |
| 12  | col C                    |
| 13  | col D                    |
| 14  | col E                    |
| 15  | col F                    |
| 16  | col G                    |
| 17  | col H                    |
| 18  | col I                    |
| 19  | col J                    |
| 20  | col K                    |
| 21  | col L                    |
| 22  | col M                    |
| 23  | col N                    |
| 24  | col O                    |
| 25  | col P                    |
| 26  | col Q                    |
| 27  | col R                    |
| 28  | n/a (Q2 LED)             |
| 29  | n/a (Q1 LED)             |
| 30  | GND (LEDs and fn switch) |

## US/UK layout
|   |   A  |   B  |   C  |   D  |   E  |   F  |   G  |   H  |  I  |  J  |  K   |   L   |  M  |   N  |   O  |   P  |   Q  |
|---|------|------|------|------|------|------|------|------|-----|-----|------|-------|-----|------|------|------|------|
| 1 |      | PAUS |      |  DEL |    0 |    9 |    8 | BSPC | 7   | TAB | Q    | 2     | 1   |      |      |      |      |
| 2 |      | PGUP |      |  F12 | LBRC | MINS | RBRC |  INS | Y   | F5  | F3   | W     | 4   |      | F6   |      |      |
| 3 |      | HOME |      | CALC |    P |    O |    I |      | U   | R   | E    | CAPS  | 3   |      | T    |      |      |
| 4 |      | SLCK |      |  ENT | SCLN |    L |    K | BSLS | J   | F   | D    | (NUBS)| A   |      | LGUI |      |      |
| 5 |      |      | RALT |  APP | SLSH | QUOT |      | LEFT | H   | G   | F4   | S     | ESC |      |      | LALT |      |
| 6 |      | END  | RSFT | PGDN |(NUHS)|  DOT | COMM |      | M   | V   | C    | X     | Z   | LSFT |      |      |      | 
| 7 | LCTL | RGHT |      |   UP | DOWN |      |      | RSPC | N   | B   | LSPC |       |     |      |      |      | RCTL |
| 8 |      | PSCR |      |  F11 |  EQL |   F9 |   F8 |  F10 | F7  | 5   | F2   | F1    | GRV |      | 6    |      |      |

## Manufacturing and improvements
### Revision 1
The board is manufactured by JLCPCB. However, there are few things to improve:
- the H1 and H2 screws don't have enough clearence
- the keyboard connector is not placed well as the cable needs to be bent (move to same location as the original board)
- add testpoints for the signals from the keyboard matrix as the original board has
- USB is at wrong place. Now it faces directly the battery. Better place would be at the top somewhere around the notch

## License
This project uses the [CERN Open Hardware Licence Version 2 - Permissive](https://ohwr.org/project/cernohl/-/wikis/Documents/CERN-OHL-version-2).
